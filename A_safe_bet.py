import sys
import numpy as np
leftmirroor = 1
rightmirror = 2
# creating grid
def create_safe(row,col):
    arr = np.zeros(shape = (row,col))
    for i,j in m_mirror:
        arr[i-1][j-1] = leftmirroor
    for i,j in n_mirror:
        arr[i-1][j-1] = rightmirror
    return arr
#checking safe is empty or not
def is_empty(safe):
    count = 0
    for i in range(row):
        for j in range(col):
            if safe[i][j] == 0:
                count += 1
    if count == col*row:
        return True
    else:
        return False

#checking if there is mirror in sub array
def is_there_mirror(subarr):
    if(subarr.count(0) == len(subarr)):
        return False
    else:
        return True

#returning the posn of mirror
def position(subarr):
    if 2 in subarr:
        return len(subarr) -subarr[::-1].index(2) - 1
    elif 1 in subarr:
        return len(subarr) - subarr[::-1].index(1)

#checking if mirror is present in column  or not
def is_there_in_coloumn(p,pos,arr):
    count = 0
    for k in range(0,pos+1):
        if arr[k][p] !=0:
            count += 1
    if(count >= 1):
        return True
    else:
        return False
def is_notsame(subarr):
    m_mirror_count = 0
    n_mirror_count = 0
    for cell in subarr:
        if cell == 1:
            m_mirror_count += 1
        if cell == 2:
            n_mirror_count += 1
    if m_mirror_count == 1 and n_mirror_count == 1:
        return True
    else:
        return False   
        
# main method to chcek the safe
def return_posn(row,col,safe):        
    li = []
    for i in range(col):
        subarr = list(safe[:, i])
        if is_notsame(subarr) :
            return "impossible"
        if is_there_mirror(subarr) :
            pos = position(subarr)
            for p in range(i+1,col):
                if is_there_in_coloumn(p,pos,safe) :
                    li.append(p)
            if pos == col-1 :
                return  0
            if len(li) != 0  :
                return  len(li), pos+1, min(li)+1

#taking inputs using command line arguements
def main():
    row = int(sys.argv[1])
    col = int(sys.argv[2])
    no_of_m_mirror = int(sys.argv[3])
    no_of_n_mirror = int(sys.argv[4])
    m_mirror=[]
    n_mirror=[]
    for i in range (5,5+(2*no_of_m_mirror)-,2):
        m_mirror.append([int(sys.argv[i]),int(sys.argv[i+1])])
    for i in range (5+(2*no_of_m_mirror)-1,5+(2*no_of_m_mirror)+(2*no_of_n_mirror),2):
        m_mirror.append([int(sys.argv[i]),int(sys.argv[i+1])])
    if is_empty(safe, row, col):
        print('Impossible')
    else:
        if return_posn(row, col, safe) == None:
            print("impossible")   
        elif return_posn(row, col, safe) == "impossible":
            print("impossible")
        elif return_posn(row,col,safe):
            for i in return_posn(row,col,safe):
                print(i,end = ' ')
        else:
            print(return_posn(row,col,safe))
main()
